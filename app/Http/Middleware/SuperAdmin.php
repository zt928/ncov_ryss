<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $admin = Auth::user()->is_admin;
        if ($admin !== '0') {
            $admins = unserialize($admin);
            if (in_array(0, $admins)) {
                return $next($request);
            }
        }
        abort(401);
    }
}
