<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AddressController extends Controller
{
    public function index()
    {
        $addresses = Address::where('user_id', Auth::id())->get();
        return view('home.address.index', compact('addresses'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $address = Address::create($data);

        if ($address) {
            $this->clear_cache();
            return redirect(route('home'))->with('success', '收货信息创建成功!');
        } else {
            return back()->with('success', '收货信息创建失败,请重试,多次失败请联系管理员!');
        }
    }

    public function destroy($id)
    {
        $del = Address::destroy($id);
        if ($del) {
            $this->clear_cache();
            return back()->with('success', '删除用户收货信息成功。');
        } else {
            return back()->with('success', '删除用户收货信息失败！！！');
        }
    }

    private function clear_cache()
    {
        Cache::forget('address_' . Auth::id());
        Cache::forget('address_first_' . Auth::id());
    }


}
