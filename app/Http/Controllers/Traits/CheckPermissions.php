<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Facades\Auth;

trait CheckPermissions
{

    private function is_mall_manager($mall_id)
    {
        $admin = Auth::user()->is_admin;
        if (Auth::user()->is_admin !== '0') {
            $admins = unserialize($admin);
            if (!in_array(0, $admins)) {
                if (!in_array($mall_id, $admins)) {
                    abort(401);
                }
            }
        }
    }

    private function is_mall_admin($mall_id)
    {
        $admin = Auth::user()->is_admin;
        if (Auth::user()->is_admin !== '0') {
            $admins = unserialize($admin);
            if (in_array(0, $admins)) {
                return true;
            }
            if (in_array($mall_id, $admins)) {
                return true;
            }
        } else {
            return false;
        }
    }

}
