<?php

namespace App\Http\Controllers;

use App\Mall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MallController extends Controller
{
    public function index()
    {
        $malls = Mall::all();
        return view('admin.mall.index', compact('malls'));
    }

    public function edit($id)
    {
        $mall = Mall::find($id);
        return view('admin.mall.edit', compact('mall'));
    }

    public function update(Request $request, $id)
    {
        $is_display = $request->input('is_display');
        $update = Mall::find($id)->update(['is_display'=> $is_display]);

        if ($update) {
            Cache::forget('mall_' . $id);
            Cache::forget('malls');
            return redirect(route('admin.mall.index'))->with('success', '操作成功');
        } else {
            return back()->with('success', '操作失败!!!!!!');
        }
    }
}
