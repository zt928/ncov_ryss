@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">收货信息</div>
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="card-body">
                        @foreach($addresses as $address)
                            <div>
                                <p>姓名: {{ $address->name }} 电话: {{ $address->tel }} 楼栋号码：{{ $address->home_number }} <a
                                        href="javascript:;" onclick="document.getElementById('address_del_{{ $address->id }}').submit();">删除</a></p>
                                <form action="{{ route('address.destroy',$address->id) }}" method="post"　id="address_del_{{ $address->id }}" style="display: none;">
                                    @csrf
                                    @method('delete')
                                </form>
                            </div>
                        @endforeach
                        <p><a href="{{route('home')}}">返回个人中心</a></p>
                    @if($addresses->count()!==0)
{{--                            <div>--}}
                                <p>您的收货信息已经提交,不需要重复提交!</p>
{{--                                <p>姓名: {{ $address->name }} 电话: {{ $address->tel }} 楼栋号码：{{ $address->home_number }}</p>--}}
{{--                                <p><a href="{{route('home')}}">返回</a></p>--}}
{{--                            </div>--}}
                        @else
                            <div class="alert alert-danger" role="alert">
                                准确的收货信息十分重要，请务必填写自己的真实姓名和电话，方便志愿者的工作。
                            </div>
                            <form action="{{route('address.store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="name">姓名</label>
                                    <input type="text" class="form-control" name="name" id="name"
                                           required >
                                    <small id="emailHelp" class="form-text text-muted">请输入自己的真实姓名</small>
                                </div>
                                <div class="form-group">
                                    <label for="tel">手机号码</label>
                                    <input type="text" class="form-control" id="tel" name="tel"
                                           required maxlength="11" minlength="11">
                                    <small id="emailHelp" class="form-text text-muted">请输入自己的手机号码</small>
                                </div>
                                <div class="form-group">
                                    <label for="home_number">楼栋号码 【例如:东区19栋1单元301】</label>
                                    <input type="text" class="form-control" id="home_number" name="home_number"
                                           required >
                                    <small id="home_number" class="form-text text-muted">例如:东区19栋1单元301</small>
                                </div>
                                <button type="submit" class="btn btn-primary">提交</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
