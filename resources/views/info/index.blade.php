@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
               @include('info._menu')
                <div class="card">
                    <div class="card-body">
                        物业电话:15971436290  社区电话:（027）61880180
                        <hr>
                        <p class="font-weight-bold">问：如何使用本系统进行团购？</p>
                        <p class="font-weight-normal">答：请先使用本人的手机号码注册，然后点击开始购物->加入购物车->提交订单->立即付款 即可。</p>
                        <p class="font-weight-bold">问：什么时候可以团？团了什么时间拿货？</p>
                        <p class="font-weight-normal">答：麦德龙每周二开团周五拿货、周六周日开团周二拿货；中百每周三开团周五拿货。具体的时间请留意群内通知。</p>
                        <p class="font-weight-bold">问：团购如何付款？</p>
                        <p class="font-weight-normal">答：选购商品加入购物车并提交订单，按系统提示操作。</p>
                        <p class="font-weight-bold">问：团购在了哪里取货？凭什么取货？</p>
                        <p class="font-weight-normal">答：每次取货的地点在小区东区岗亭，请凭你的订单号和姓名取货，排队请保持两米的距离。</p>
                        <p class="font-weight-bold">问：政府的爱心菜和冷冻肉如何购买？</p>
                        <p class="font-weight-normal">答：由于数量稀少，我们会优先满足特殊困难群体购买，多余的部分会放到系统中进行抢购，如果你身边有困难群体需要购买，请联系我们进行登记。</p>
                        <p class="font-weight-bold">问：为什么都是套餐无法单买？</p>
                        <p class="font-weight-normal">答：目前所有对接的商超都是套餐形式，我们只能按商超的来，不接受单买，因此没有的商品也没有办法。</p>
                        <p class="font-weight-bold">问：我想要的XXX商品没有，可以想办法吗？</p>
                        <p class="font-weight-normal">答：参考上一条。</p>
                        <p class="font-weight-bold">问：为什么加收款员好友没有通过？</p>
                        <p class="font-weight-normal">答：收款员一个人对接多人，请耐心等待，肯定会通过的。</p>
                        <p class="font-weight-bold">问：为什么我已经付款了系统显示我未付款？</p>
                        <p class="font-weight-normal">答：每个商超收款员是唯一的，需要边对账边在后台确认收款，因此存在一定时间的延迟，请耐心等待。</p>
                        <p class="font-weight-bold">问：我买的商品不想要了可以退吗？</p>
                        <p class="font-weight-normal">答：未付款之前的订单请自行删除即可，已经付款的订单不接受退货。</p>
                        <p class="font-weight-bold">问：购买的东西有质量问题如何售后？</p>
                        <p class="font-weight-normal">答：请将包装和标签拍照然后联系收款员处理。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
