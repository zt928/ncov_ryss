@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success" role="alert">
                {{ session('success') }}
            </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.index') }}">后台管理</a> / 商户管理</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">商户名称</th>
                                <th scope="col">营业状态</th>
                                <th scope="col">是否显示</th>
                                <th scope="col">微信二维码</th>
                                <th scope="col">创建时间</th>
                                <th scope="col">操作</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($malls as $mall)
                                <tr>
                                    <td>{{ $mall->id}}</td>
                                    <td>{{ $mall->name}}</td>
                                    <td>{{ $mall->is_show}}</td>
                                    <td>{{ $mall->is_display}}</td>
                                    <td><a href="{{ $mall->pay_qrcode}}" target="_blank">查看</a></td>
                                    <td>{{ $mall->created_at}}</td>
                                    <td ><a href="{{ route('admin.mall.edit',$mall->id) }}">编辑</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
