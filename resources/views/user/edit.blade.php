@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><a href="{{ route('admin.user.index') }}"><a href="{{ route('admin.user.index') }}">用户管理</a> / 修改密码</a></div>
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="card-body">
                        <form action="{{route('admin.user.update',$user->id)}}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="exampleInputPassword1">用户名</label>
                                <input type="text" class="form-control" id="exampleInputPassword1" name="name" value="{{ $user->name }}" disabled >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword2">新密码</label>
                                <input type="text" class="form-control @error('password') is-invalid @enderror" id="exampleInputPassword2" name="password"  placeholder="修改前请通过手机号码确认用户身份" minlength="8" autocomplete="off">
                                <small class="text-danger">*不改密码请留空</small>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="0"
                                           name="is_admin[]" @if(in_array(0,$admins)) checked @endif>
                                    <label class="form-check-label" for="inlineCheckbox1">超级管理员</label>
                                </div>
                                @foreach($malls as $mall)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" id="Checkbox{{ $mall->id }}" value="{{ $mall->id }}"
                                               name="is_admin[]"  @if(in_array($mall->id,$admins)) checked @endif>
                                        <label class="form-check-label" for="Checkbox{{ $mall->id }}">{{ $mall->name }}</label>
                                    </div>
                                @endforeach
                            </div>
                            <button type="submit" class="btn btn-primary">提交</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
